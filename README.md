# Ficbook Client - Unofficial Ficbook Client on Python 3.6

# Requirements:

* Python 3.6
* pip (Python package manager)
* requests

# Ficbook Search API Wrapper

## Example usage:

```
# Import search module 

from ficbook.search import FicbookSearchAPI
from ficbook.download import downloadFanfic

# Initialize search API

fapi = FicbookSearchAPI()

# Search

fanfics = fapi.fanfics("<some fanfic title>")

# Download

for fanfic in fanfics:
    download.download_fanfic(fanfic, "<desired file format: e.g: fb2>")

```

# PyQt5 based client

```
pip install --user ficbook/ ficbook-client/
ficbook-client
```
