'''
This module is a part of Ficbook Client

Provides download_fanfic procedure
'''

import os
import requests
from ficbook.constants import FICBOOK_BASE_URL
from ficbook.models import Fanfic


def download_fanfic(fobject: Fanfic, fformat: str, fdir: str = ""):

    """
    Downloads fanfic in specified format into specified directory
    param: fobject: Instance of Fanfic model
    param: fformat: String with format
    param: fdir: String with directory
    """

    if fformat not in ["txt", "epub", "pdf", "fb2"]:
        raise ValueError("Invalid fanfic format")

    if not isinstance(fobject, Fanfic):
        raise TypeError("Invalid fobject passed")

    if not fdir:
        fdir = os.path.join(os.environ.get("HOME"), "Downloads")

    if not os.path.exists(fdir):
        raise ValueError("Fdir does not exist")

    fid, fname = (fobject.id, "%s - %s" % (
        fobject.title, fobject.author_nickname)
    )

    furl = "%s/fanfic_download/%s/%s" % (FICBOOK_BASE_URL, fformat, fid)

    res = requests.get(furl)
    res.raise_for_status()

    with open(os.path.join(fdir, "%s.%s" % (fname, fformat)), "w") as fout:
        fout.write(res.content.decode())
        fout.close()

    return fname
