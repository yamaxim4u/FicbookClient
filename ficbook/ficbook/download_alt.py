'''
This module is a part of Ficbook Client

Provides alternative download_fanfic procedure
Using 'lxml.html', 're', and others modules
'''

import os
import requests
from lxml import html
from ficbook.constants import FICBOOK_BASE_URL
from ficbook.models import Fanfic

def get_html_tree(url: str):

    if not FICBOOK_BASE_URL in url:
        url = FICBOOK_BASE_URL + url

    res = requests.get(url)
    res.raise_for_status()
    tree = html.fromstring(res.content)
    return tree


def get_parts(url: str):

    returned = []

    tree = get_html_tree(url)

    lis = tree.xpath('//*/ul[@class="list-unstyled table-of-contents"]/li')

    if not lis:
        return returned

    for li in lis:
        returned.append({
            'title': li.xpath('a/text()')[0],
            'url': li.xpath('a/@href')[0]})

    return returned

def get_single_part_page_text_content(url: str):

    returned = ""

    tree = get_html_tree(url)

    content = content = tree.xpath('//*/div[@id="content"]')

    if not content:
        return returned

    returned = content[0].text_content()

    return returned


def download_fanfic(fobject: Fanfic, fformat: str, fdir: str = ""):

    """
    Downloads fanfic in specified format into specified directory
    param: fobject: Instance of Fanfic model
    param: fformat: String with format
    param: fdir: String with directory
    """

    if fformat not in ["txt"]:
        raise ValueError("Invalid fanfic format")

    if not isinstance(fobject, Fanfic):
        raise TypeError("Invalid fobject passed")

    if not fdir:
        fdir = os.path.join(os.environ.get("HOME"), "Downloads")

    if not os.path.exists(fdir):
        raise ValueError("Fdir does not exist")

    fid, fname = (fobject.id, "%s - %s" % (
        fobject.title, fobject.author_nickname)
    )

    furl = "%s/readfic/%s" % (FICBOOK_BASE_URL, fid)

    fparts = get_parts(furl)

    fcontent = []

    for fpart in fparts:
        fcontent.append({
            'title': fpart['title'],
            'content': get_single_part_page_text_content(fpart['url'])})

    print(fcontent)

    with open(os.path.join(fdir, "%s.%s" % (fname, fformat)), "w") as fout:
        for fcontent_slice in fcontent:
            fout.write(
                fcontent_slice['title'] + "\n\n" + fcontent_slice['content'] + "\n\n")
        fout.close()

    return fname
