'''
This module is a part of Ficbook Client

Provides common models for Ficbook API entities
'''

from ficbook.constants import FICBOOK_BASE_URL


class BaseModel(object):

    '''
    Base model
    '''

    def __init__(self, data):
        for key, value in data.items():
            setattr(self, key, value)
        # if self.__class__.__name__.lower() == self.type:
        #    raise ValueError("Wrnog entity type: %s" % self.tyoe)


class Size(BaseModel):

    '''
    Handles Size entity
    '''

    def __str__(self):
        return "<Size: Title: %s, Adult Only: %s>" % (
            self.title, str(self.is_adult)
        )


class Rating(BaseModel):

    '''
    Handles Rating entity
    '''

    def __str__(self):
        return "<Size: Title: %s>" % self.title


class Genre(BaseModel):

    '''
    Handles Genre entity
    '''

    def __str__(self):
        return "<Genre: Title: %s>" % self.title


class Warning(BaseModel):

    '''
    Handles Warning entity
    '''

    def __str__(self):
        return "<FWarning: Title: %s, Adult Only: %s>" % (
            self.title, str(self.is_adult)
        )


class Fandom(BaseModel):

    '''
    Handles Fandom entity
    '''

    def __init__(self, *args):
        super(Fandom, self).__init__(*args)

        self.url = "%s/fanfiction/%s/%s" % (
            FICBOOK_BASE_URL, self.group_crazy_title, self.slug
        )

    def __str__(self):
        return "<Fandom: Title: %s, Second Title: %s, Group: %s>" % (
            self.title, self.sec_title, self.group_title
        )


class Fanfic(BaseModel):

    '''
    Handles Fanfic entity
    '''

    def __init__(self, *args):
        super(Fanfic, self).__init__(*args)

        self.url = "%s/readfic/%s" % (FICBOOK_BASE_URL, self.id)
        self.fandoms = [Fandom(f) for f in self.fandoms] if hasattr(self, "fandoms") else []
        self.size = Size(self.size) if hasattr(self, "size") else []
        self.rating = Rating(self.rating) if hasattr(self, "rating") else []
        self.genres = [Genre(g) for g in self.genres] if hasattr(self, "genres") else []
        self.warnings = [Warning(w) for w in self.warnings] if hasattr(self, "warnings") else []

    def __str__(self):
        return "<Fanfic: Title %s, Marks Plus: %s, Author Nickname: %s>" % (
            self.title, self.marks_plus, self.author_nickname
        )


class Request(BaseModel):

    '''
    Handles Request entity
    '''

    def __init__(self, *args):
        super(Request, self).__init__(*args)

        self.url = "%s/requests/%s" % (constants.FICBOOK_BASE_URL, self.id)
        self.fandoms = [Fandom(f) for f in self.fandoms]
        self.ratings = [Rating(r) for r in self.ratings]
        self.genres = [Genre(g) for g in self.genres]
        self.warnings = [Warning(w) for w in self.warnings]

    def __str__(self, *args):
        return "<Request: Title: %s>" % self.title
