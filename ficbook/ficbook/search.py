'''
This module is a part of Ficbook Client

Provides Ficbook Search API Wrapper
'''


import requests
from urllib.parse import urlencode
from ficbook.constants import FICBOOK_BASE_URL
from ficbook.models import Fandom, Request, Fanfic


class FicbookError(Exception):

    def __init(self, message):
        super().__init__(message)


class FicbookSearchAPI(object):

    url = "%s/search" % FICBOOK_BASE_URL

    def __make_query__(self, query_data: dict, section: str = ""):
        """
        Makes GET query
        param: query_data: Dictionary with query data
        param: section: String with section
        return: String with complete GET query
        """
        query = urlencode(query_data)
        if section:
            return "%s/%s?%s" % (self.url, section, query)
        return "%s?%s" % (self.url, query)

    def __exec_query__(self, query: str):
        """
        Executes builded query
        param: query: String with complete GET query
        return: response_data: Dictionary with query result data
        """
        response = requests.get(query)
        response.raise_for_status()
        response_data = response.json()
        if response_data.get("error"):
            raise FicbookError("Ficbook Error: %s" % response_data["error"])
        return response_data

    def __search_base__(self, section: str, **kwargs):
        """
        Wraps __make__- and __exec__- query functions and handles response data
        param: section: String with section
        return: Dictionary with extracted query result data
        """
        query = self.__make_query__(kwargs, section)
        response = self.__exec_query__(query)
        return response["data"]["data"] if section else response["data"]

    def all(self, term: str):
        """
        Shortcut for "search all"
        param: term: String with term to search
        return: Dictionary with query result data body
        """
        return self.__search_base__(None, term=term)

    def fandoms(self, term: str, page: int =1):
        """
        Shortcut for "search fandoms"
        param: term: String with term to search
        return: List with Fandom instances
        """
        return [
            Fandom(f) for f in
            self.__search_base__("fandom", term=term, page=page)
        ]

    def requests(self, term: str, page: int =1):
        """
        Shortcut for "search requests"
        param: term: String with term to search
        return: List with Request instances
        """
        return [
            Request(r) for r in
            self.__search_base__("fanficrequest", term=term, page=page)
        ]

    def fanfics(self, term, page: int =1):
        """
        Shortcut for "search fanfics"
        param: term: String with term to search
        return: List with Fanfic instances
        """
        return [
            Fanfic(f) for f in
            self.__search_base__("fanfic", term=term, page=page)
        ]
