from setuptools import setup, find_packages

setup(name='ficbook',
      version='0.0.1',
      description='Python wrapper for ficbook search API',
      author='jedi2light',
      license='MIT',
      packages=find_packages(),
      install_requires=["requests"],
      zip_safe=True
)
