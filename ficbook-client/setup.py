from setuptools import setup, find_packages

setup(name='ficbook-client',
      version='0.0.1',
      description='Python wrapper for ficbook search API - PyQt5 based client',
      author='jedi2light',
      license='MIT',
      packages=find_packages(),
      install_requires=["ficbook"],
      zip_safe=True,
      entry_points={
          'console_scripts': [
              'ficbook-client = ficbook_client.client:main'
              ]
          }
      )
