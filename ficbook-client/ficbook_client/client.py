'''
Thist module is a main part of Ficbook Client

'''

import sys
import os
import requests
from PyQt5 import QtWidgets
from ficbook.search import FicbookSearchAPI
from ficbook.models import Fanfic
if os.environ.get('FICBOOK_CLIENT_DOWNLOAD_ALT') == '1':
    from ficbook.download_alt import download_fanfic
else:
    from ficbook.download import download_fanfic
from ficbook_client import design

FANFIC_DESCRIPTION_TEMPLATE = '''
<p>
    <h3>
        <a href="{f_url}">{f_title}</a>
        <span style="color:green;">   {f_marks}+</span>
        <span style="color:blue;">   {f_premium}</span>
    </h3
    <strong>Author: </strong>{f_author}
</p>
<p>
    <strong>Fandom(s): </strong>{f_fandoms} <br/>
    <strong>Pairing and characters: </strong>{f_pairings} <br/>
    <strong>Raiting: </strong>{f_raiting} <br/>
    <strong>Genres: </strong>{f_genres} <br/>
    <strong>Warnings: </strong>{f_warnings} <br/>
    <strong>Size: </strong>{f_size} <br/>
</p>
<hr/>
<p>
    {f_description}
</p>
'''


class FicbookClient(QtWidgets.QMainWindow, design.Ui_MainWindow):

    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.fapi = FicbookSearchAPI()

        self.searchButton.clicked.connect(self.ficSearch)
        self.fanficList.itemClicked.connect(self.ficDescription)
        self.downloadButton.clicked.connect(self.ficDownload)

    def ficSearch(self):
        searchQueryText = self.searchQuery.text()
        if len(searchQueryText) < 2:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "Query must contain at least 2 characters"
            )
            return
        try:
            fanfics = self.fapi.fanfics(searchQueryText)
        except requests.exceptions.ConnectionError:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "Unable to connect"
            )
            return
        except requests.exceptions.HTTPError:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "HTTP Error"
            )
            return
        self.fanficList.clear()
        self.fanficDesc.clear()
        self.fanfics = fanfics
        for fanfic in self.fanfics:
            self.fanficList.addItem("%s - %s ( %d+ )" % (
                fanfic.author_nickname,
                fanfic.title,
                fanfic.marks_plus)
            )

    def ficDownload(self):
        fanfic = self.fanficList.currentRow()
        if fanfic < 0:
            QtWidgets.QMessageBox.information(
                None, "Error:", "Fanfic not selected"
            )
            return
        fanfic = self.fanfics[fanfic]
        downloadDir = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "Select fanfic download directory"
        )
        if len(downloadDir) < 1:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "Download directory not selected"
            )
            return
        if self.txtChoise.isChecked():
            fileFormat = "txt"
        elif self.fb2Choise.isChecked():
            fileFormat = "fb2"
        elif self.epubChoise.isChecked():
            fileFormat = "epub"
        elif self.pdfChoise.isChecked():
            fileFormat = "pdf"
        else:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "File format not selected"
            )
            return
        QtWidgets.QMessageBox.information(
            None,
            "Client Info:", "Fanfic will be downloaded"
        )
        self.downloadButton.setDisabled(True)
        try:
            fileName = download_fanfic(
                fanfic, fileFormat, downloadDir
            )
        except requests.exceptions.ConnectionError:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "Unable to connect"
            )
            return
        except requests.exceptions.HTTPError:
            QtWidgets.QMessageBox.information(
                None,
                "Error:", "HTTP Error"
            )
            return
        QtWidgets.QMessageBox.information(
            None,
            "Client Info:", "Fanfic downloaded as '%s.%s'" % (
                os.path.join(downloadDir, fileName), fileFormat
            )
        )
        self.downloadButton.setDisabled(False)


    def ficDescription(self, fanficItem):
        fanfic = self.fanfics[self.fanficList.row(fanficItem)]
        self.fanficDesc.setText(FANFIC_DESCRIPTION_TEMPLATE.format(
            **{
                "f_title": fanfic.title,
                "f_url": fanfic.url,
                "f_marks": fanfic.marks_plus,
                "f_author": fanfic.author_nickname,
                "f_fandoms": ', '.join([str(f.title) for f in fanfic.fandoms]),
                "f_pairings": ', '.join([str(p) for p in fanfic.pairings]),
                "f_raiting": fanfic.rating.title,
                "f_genres": ', '.join([str(g.title) for g in fanfic.genres]),
                "f_warnings": ', '.join([str(w.title) for w in fanfic.warnings]),
                "f_size": fanfic.size.title,
                "f_premium": "Yes" if fanfic.is_premium else "",
                "f_description": fanfic.description
            }
        ))


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = FicbookClient()
    window.show()
    app.exec_()


if __name__ == "__main__":
    main()
